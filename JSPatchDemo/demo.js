

defineClass('ViewController : UIViewController < UITableViewDataSource , UITableViewDelegate >',{
    viewDidLoad:function (){
        self.super().viewDidLoad();
        var tableView = require('UITableView').alloc().initWithFrame_style({x:0, y:0, width:self.view().frame().width, height:self.view().frame().height},0);
            tableView.setDelegate(self);
            tableView.setDataSource(self);
        self.view().addSubview(tableView);
    },
    numberOfSectionsInTableView: function(tableView) {
        return 1;
    },
    tableView_numberOfRowsInSection: function(tableView, section) {
        return 3;
    },
    tableView_cellForRowAtIndexPath: function(tableView, indexPath) {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if (!cell) {
            cell = require('UITableViewCell').alloc().initWithStyle_reuseIdentifier(0, "cell")
        }
        cell.textLabel().setText('aa');
        return cell
    },
});