//
//  AppDelegate.h
//  JSPatchDemo
//
//  Created by banana on 21/4/16.
//  Copyright © 2016年 banana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

