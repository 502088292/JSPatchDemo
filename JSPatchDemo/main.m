//
//  main.m
//  JSPatchDemo
//
//  Created by banana on 21/4/16.
//  Copyright © 2016年 banana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
